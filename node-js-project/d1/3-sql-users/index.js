import http from 'http';
import { UserService } from './user.js';

const HOST_NAME = '127.0.0.1';
const PORT = 3000;

const server = http.createServer(async (req, res) => {
    const users = await UserService.getAllUsers();
    
    res.statusCode = 200;
    //res.setHeader('Content-Type', 'text/plain');
    //res.end('Hello World');

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(users));
});

server.listen(PORT, HOST_NAME, () => {
    console.log(`server running at http://${HOST_NAME}:${PORT}`)
})