import db from './db.js';

export class UserService {
    static getAllUsers = async () => {
        //logic
        /*
        db.execute(
            'SELECT * FROM users',
            (err, result) => {
                if (err) throw err;

                console.log(result);
            }
        )
        */

        /*
        return db.promise().execute(`SELECT * FROM user`)
            .then(([result, fields]) => {
                //console.log('success.', result);

                return result;
            })
            .catch(err => {
                console.log(err);
            })
        */

        return db.promise().execute(
            `SELECT * FROM user WHERE firstName = ? AND birthDate = ? LIMIT 1`,
            ['John', '1978-12-22']
        )
            .then(([result, fields]) => {
                //console.log('success.', result);
                console.log(`${result[0].birthDate} ${result[0].firstName}`);
                return result;
            })
            .catch(err => {
                console.log(err);
            })
    }
}