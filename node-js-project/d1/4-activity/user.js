import db from './db.js';

export class UserService {
    static getAllUsers = async () => {
        return db.promise().execute(
            `SELECT firstName, childhoodDream FROM user WHERE firstName = ?`,
            ['Rae Dan Emmanuel']
        )
            .then(([result, fields]) => {
                //console.log(`HELLO WORLD!`);
                console.log(`${result[0].firstName} - ${result[0].childhoodDream}`);
                return result;
            })
            .catch(err => {
                console.log(err);
            })
    }
}