import mysql from 'mysql2';

const config = {
    host: 'localhost',
    port: 3306,
    database: 'nodeshop',
    user: 'dev',
    password: 'hppywknd',
    connectionLimit: 10
};

const pool = mysql.createPool(config);

export default pool;