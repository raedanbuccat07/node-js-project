export interface Product {
    id?: number;
    name: string;
    price: number;
    status: boolean;
    description: string;
    imageUrl: string;
    stockQty: number;
    categoryId: number;
}