import { Router } from 'express';

import { CatalogController } from '../controllers/catalog.controller';

const router = Router();

router.get('/', CatalogController.getAllProducts);

router.get('/:id([0-9]+)', CatalogController.getProductById);

router.post('/', CatalogController.addProduct);

router.patch('/:id([0-9]+)', CatalogController.updateProduct);

router.delete('/:id([0-9]+)', CatalogController.deleteProduct);

export default router;