import { Router } from 'express';

import { WishlistController } from '../controllers/wishlist.controller';

const router = Router();

router.get('/', WishlistController.getAllWishlists);

router.get('/:id([0-9]+)', WishlistController.getWishlistById);

router.post('/', WishlistController.addWishlist);

router.patch('/:id([0-9]+)', WishlistController.updateWishlist);

router.delete('/:id([0-9]+)', WishlistController.deleteWishlist);

export default router;