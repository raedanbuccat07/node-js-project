import { Router } from 'express';

import userRouter from './user.routes';
import authRouter from './auth.routes';
import productRouter from './product.routes';
import wishlistRouter from './wishlist.routes';

import { handleError } from '../middlewares/error-handler.middleware';

const router = Router();

router.use('/users', userRouter);
router.use('/auth', authRouter);
router.use('/products', productRouter);
router.use('/wishlist', wishlistRouter);

router.use(handleError);

export default router;