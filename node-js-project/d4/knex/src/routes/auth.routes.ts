import { Router, Request, Response, NextFunction } from "express";

const router = Router();

router.get('/profile',
    [(req: Request, res: Response, next: NextFunction) => {
        // pre request middleware
        if (req.headers.authorization === 'secrectp@ssw0rd') {
            next();
        } else {
            res.status(401).send('Not permitted.');
        }
    },
    (req: Request, res: Response, next: NextFunction) => {
        // pre request middleware
        if (req.headers.access === 'profile') {
            next();
        } else {
            res.status(403).send();
        }
    }],
    (req: Request, res: Response) => {
        res.send('Welcome!');
    });

export default router;