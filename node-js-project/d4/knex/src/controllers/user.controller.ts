import { Request, Response, NextFunction } from "express";

import { UserService } from "../services/user.service";
import { User } from "../models/user.model";

export class UserController {
    static add = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const user: User = req.body;
            const response = await UserService.add(user);
            console.log(response);

            return res.status(201).json({ msg: 'Successfully added!', user });
        } catch (error) {
            next(error);
        }
    }

    static getAll = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const response = await UserService.getAll();

            return res.status(201).json(response);
        } catch (error) {
            next(error);
        }
    }
}