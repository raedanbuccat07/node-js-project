import { NextFunction, Request, Response } from 'express';
import { Product } from '../models/product.model';
import { CatalogService } from '../services/catalog.service';

export class CatalogController {
    static getAllProducts = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const products = await CatalogService.getAllProducts();
            return res.status(200).json(products);
        } catch (error) {
            next(error);
        }
    }

    static getProductById = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const product = await CatalogService.getProductById(Number(req.params.id));
            return res.status(200).json(product);
        } catch (error) {
            next(error);
        }
    }

    static addProduct = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const product: Product = req.body;
            const response = await CatalogService.addProduct(product);

            return res.status(201).json({
                msg: 'Successfully added!',
                product,
                response
            });
        } catch (error) {
            next(error);
        }
    }

    static updateProduct = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const product: Product = req.body;

            // Update the product if existing
            await CatalogService.updateProduct(Number(req.params.id), product);
            return res.status(204).send();
        } catch (error) {
            next(error);
        }
    }

    static deleteProduct = async (req: Request, res: Response, next: NextFunction) => {
        try {
            // Delete the product
            await CatalogService.deleteProduct(Number(req.params.id));
            return res.status(204).send();
        } catch (error) {
            next(error);
        }
    }
}