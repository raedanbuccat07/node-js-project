import { NextFunction, Request, Response } from 'express';
import { Wishlist } from '../models/wishlist.model';
import { WishlistService } from '../services/wishlist.service';

export class WishlistController {
    static getAllWishlists = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const wishlists = await WishlistService.getAllWishlists();
            return res.status(200).json(wishlists);
        } catch (error) {
            next(error);
        }
    }

    static getWishlistById = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const wishlist = await WishlistService.getWishlistById(Number(req.params.id));
            return res.status(200).json(wishlist);
        } catch (error) {
            next(error);
        }
    }

    static getWishlistByIdUpdate = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const wishlist = await WishlistService.getWishlistById(Number(req.params.id));
            return res.status(200).json(wishlist);
        } catch (error) {
            next(error);
        }
    }

    static addWishlist = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const wishlist: Wishlist = req.body;
            const response = await WishlistService.addWishlist(wishlist);

            return res.status(201).json({
                msg: 'Successfully added!',
                wishlist,
                response
            });
        } catch (error) {
            next(error);
        }
    }

    static updateWishlist = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const wishlist: Wishlist = req.body;
            // Update the wishlist if existing
            await WishlistService.updateWishlist(Number(req.params.id), wishlist);

            return res.status(200).json({msg: 'Successfully updated wishlist!'});
        } catch (error) {
            next(error);
        }
    }

    static deleteWishlist = async (req: Request, res: Response, next: NextFunction) => {
        try {
            // Delete the wishlist
            await WishlistService.deleteWishlist(Number(req.params.id));
            return res.status(200).json({msg: 'Successfully deleted wishlist!'});
        } catch (error) {
            next(error);
        }
    }
}