import db from '../utils/database';
import knex from '../utils/knex.database';
import { User } from '../models/user.model';

export class UserRepository {
    static addUser = async (user: User) => {
        try {
            /* return knexInstance.insert({
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                password: user.password
            })
                .into('customer'); */

            return knex('customer').insert(user);
        } catch (error) {
            throw error;
        }
    }

    static getAllUsers = async () => {
        try {
            /* return knexInstance.select()
                .from<User>('customer'); */
            // return knexInstance<User>('customer').select();            
            // return knexInstance<User>('customer').select('email', 'firstName', 'lastName');
            return knex<User>('customer').select().column('email', 'firstName', 'lastName');
        } catch (error) {
            throw error;
        }
    }

    static updateUser = async (id: number, user: User) => {
        try {
            // logic to fetch and check if existing

            return knex('customer').update({
                firstName: user.firstName,
                lastName: user.lastName,
                email: user.email,
                password: user.password
            })
                .where('id', id);
        } catch (error) {
            throw error;
        }
    }
}