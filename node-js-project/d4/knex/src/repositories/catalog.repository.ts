import { Product } from '../models/product.model';
import knex from '../utils/knex.database';

export class CatalogRepository {
    static getAllProducts = async () => {
        return knex<Product>('product')
            .select([
                'product.id',
                'product.name',
                'imageUrl',
                'categoryId',
                'category.name as categoryName'
            ])
            .innerJoin('category', 'product.categoryId', 'category.id');
    }

    static getProductById = async (id: number) => {
        const product = await knex<Product>('product')
            .where('id', id)
            .first();

        if (!product) throw new Error('Not found!');

        return product;
    }

    static addProduct = async (product: Product) => {
        return knex<Product>('product')
            .insert(product);
    }

    static updateProduct = async (id: number, product: Product) => {
        return knex<Product>('product')
            .where('id', id)
            .update(product);
    }

    static deleteProduct = async (id: number) => {
        return knex<Product>('product')
            .where('id', id)
            .del();
    }
}