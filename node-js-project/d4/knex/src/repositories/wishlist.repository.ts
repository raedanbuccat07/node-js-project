import { Wishlist } from '../models/wishlist.model';
import knex from '../utils/knex.database';

export class WishlistRepository {
    static getAllWishlists = async () => {
        return knex<Wishlist>('product')
            .select([
                'wishlist.id',
                'wishlist.customerId as customerId',
                'customer.firstName', 
                'customer.lastName',
                'product.id as productId',
                'product.name',
                'imageUrl',
                'categoryId'
            ])
            .innerJoin('wishlist', 'product.id', 'wishlist.productId')
            .innerJoin('customer', 'customer.id', 'wishlist.customerId');
    }

    static getWishlistById = async (id: number) => {
        const wishlist = await knex<Wishlist>('wishlist')
            .select([
                'wishlist.id',
                'wishlist.customerId as customerId',
                'customer.firstName', 
                'customer.lastName',
                'product.id as productId',
                'product.name',
                'imageUrl',
                'categoryId'
            ])
            .innerJoin('product', 'product.id', 'wishlist.productId')
            .innerJoin('customer', 'customer.id', 'wishlist.customerId')
            .where('wishlist.id', id)
            .first();

        if (!wishlist) throw new Error('Not found!');

        return wishlist;
    }

    static getWishlistByIdUpdate = async (id: number) => {
        const wishlist = await knex<Wishlist>('wishlist')
            .where('id', id)
            .first();

        if (!wishlist) throw new Error('Not found!');

        return wishlist;
    }

    static addWishlist = async (wishlist: Wishlist) => {
        return knex<Wishlist>('wishlist')
            .insert(wishlist);
    }

    static updateWishlist = async (id: number, wishlist: Wishlist) => {
        return knex<Wishlist>('wishlist')
            .where('id', id)
            .update(wishlist);
    }

    static deleteWishlist = async (id: number) => {
        return knex<Wishlist>('wishlist')
            .where('id', id)
            .del();
    }
}