import { knex } from 'knex';

import knexConfig from '../configs/knexfile';
import config from '../configs/config';

export default knex(knexConfig[config.DEFAULTS.ENV]);