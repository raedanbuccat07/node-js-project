import { Wishlist } from '../models/wishlist.model';
import { WishlistRepository } from '../repositories/wishlist.repository';

export class WishlistService {
    static getAllWishlists = async () => {
        return WishlistRepository.getAllWishlists();
    }

    static getWishlistById = async (id: number) => {
        return WishlistRepository.getWishlistById(id);
    }

    static getWishlistByIdUpdate = async (id: number) => {
        return WishlistRepository.getWishlistByIdUpdate(id);
    }

    static addWishlist = async (wishlist: Wishlist) => {
        return WishlistRepository.addWishlist(wishlist);
    }

    static updateWishlist = async (id: number, wishlistDto: Wishlist) => {
        // Check and fetch the product if existing
        const wishlist = await WishlistRepository.getWishlistByIdUpdate(id);

        // Map product DTO to product object
        wishlist.customerId = wishlistDto.customerId;
        wishlist.productId = wishlistDto.productId;

        // Update the product if existing
        return WishlistRepository.updateWishlist(id, wishlist);
    }

    static deleteWishlist = async (id: number) => {
        // Check if the product is existing
        await WishlistRepository.getWishlistById(id);

        // Delete the product
        return WishlistRepository.deleteWishlist(id);
    }
}