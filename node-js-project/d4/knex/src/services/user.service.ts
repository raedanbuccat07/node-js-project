import bcrypt from 'bcryptjs';

import { User } from '../models/user.model';
import { UserRepository } from '../repositories/user.repository';

export class UserService {
    static add = async (user: User) => {
        if (!user.firstName || !user.lastName || !user.password || !user.email) {
            throw new Error('First name, last name, email and password are required fields.');
        }

        user.password = bcrypt.hashSync(user.password);

        return UserRepository.addUser(user);
    }

    static getAll = async () => {
        return UserRepository.getAllUsers();
    }
}