import { Product } from '../models/product.model';
import { CatalogRepository } from '../repositories/catalog.repository';

export class CatalogService {
    static getAllProducts = async () => {
        return CatalogRepository.getAllProducts();
    }

    static getProductById = async (id: number) => {
        return CatalogRepository.getProductById(id);
    }

    static addProduct = async (product: Product) => {
        return CatalogRepository.addProduct(product);
    }

    static updateProduct = async (id: number, productDto: Product) => {
        // Check and fetch the product if existing
        const product = await CatalogRepository.getProductById(id);

        // Map product DTO to product object
        product.name = productDto.name;
        product.price = productDto.price;
        product.status = productDto.status;
        product.description = productDto.description;
        product.imageUrl = productDto.imageUrl;
        product.stockQty = productDto.stockQty;
        product.categoryId = productDto.categoryId;

        // Update the product if existing
        return CatalogRepository.updateProduct(id, product);
    }

    static deleteProduct = async (id: number) => {
        // Check if the product is existing
        await CatalogRepository.getProductById(id);

        // Delete the product
        return CatalogRepository.deleteProduct(id);
    }
}