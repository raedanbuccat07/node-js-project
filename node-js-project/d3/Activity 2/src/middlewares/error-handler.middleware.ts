import { NextFunction, Request, Response } from "express";

export const handleError = (err: Error, req: Request, res: Response, next: NextFunction) => {
    console.error(err);
    res.status(500).send({ msg: err.message });
}