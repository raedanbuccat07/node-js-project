export interface User {
    firstName: string;
    middleName: string;
    lastName: string;
    birthDate: Date;
    email: string;
    password: string;
    confirmPassword: string;
    phoneNumber: string;
}