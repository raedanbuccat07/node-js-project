import db from '../utils/database';
import { User } from '../models/user.model';

export class UserRepository {
    static addUser = async (user: User) => {
        try {
            return db.promise().execute(
                `INSERT INTO register (firstName, middleName, lastName, birthDate, email, password, phoneNumber) VALUES (?, ?, ?, ?, ?, ?, ?)`,
                [user.firstName, user.middleName, user.lastName, user.birthDate, user.email, user.password, user.phoneNumber]
            )
                .then(([result, fields]) => {
                    return result;
                })
                .catch(err => {
                    throw err;
                });
        } catch (error) {
            throw error;
        }

    }
}