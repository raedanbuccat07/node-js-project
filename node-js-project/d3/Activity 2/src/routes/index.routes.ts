import { Router } from 'express';

import userRouter from './user.routes';
import authRouter from './auth.routes';

import { handleError } from '../middlewares/error-handler.middleware';

const router = Router();

router.use('/users', userRouter);
router.use('/auth', authRouter);
router.use(handleError);

export default router;