import { Router } from 'express';

const router = Router();

const users = [
    {
        id: 1,
        name: 'John',
        age: 20
    },
    {
        id: 2,
        name: 'Jane',
        age: 12
    },
    {
        id: 3,
        name: 'Juan',
        age: 34
    }
];

router.get('/', (req, res) => {
    res.json(users);
});

router.get('/:userId([0-9]+)', (req, res) => {
    const user = users.find(user => user.id === Number(req.params.userId));
    if (!user) res.status(404).json({ msg: 'User not found.' });

    res.json(user);
});



router.patch('/:userId([0-9]+)', (req, res) => {
    const user = users.find(user => user.id === Number(req.params.userId));
    if (!user) res.status(404).json({ msg: 'User not found.' });

    // Add logic

    // res.status(204).json({ msg: 'Successfully updated!', user: req.body });
    res.status(200).json({ msg: 'Successfully updated!', user: req.body });
});

router.delete('/:userId([0-9]+)', (req, res) => {
    const user = users.find(user => user.id === Number(req.params.userId));
    if (!user) res.status(404).json({ msg: 'User not found.' });

    // Add logic

    // res.status(204).json({ msg: 'Successfully updated!', user: req.body });
    res.status(204).send();
});

export default router;