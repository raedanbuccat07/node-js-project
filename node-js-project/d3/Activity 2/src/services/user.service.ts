import bcrypt from 'bcryptjs';

import { User } from '../models/user.model';
import { UserRepository } from '../repositories/user.repository';

export class UserService {


    static add = async (user: User) => {
        if (!user.firstName || !user.lastName || !user.password || !user.email || !user.confirmPassword || !user.birthDate || !user.phoneNumber) {
            throw new Error('First Name, Last Name, Birh Date, Email, Phone Number, Password, Confirm Password is required.');
        }

        if(user.password != user.confirmPassword) {
            throw new Error('Password qne Confirm Password is not the same.');
        }
      
        var date_regex = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
        if (!(date_regex.test(user.birthDate.toString()))) {
            throw new Error('Wrong date input.');
        }

        user.password = bcrypt.hashSync(user.password);
        user.confirmPassword = bcrypt.hashSync(user.confirmPassword);

        return UserRepository.addUser(user);
    }

    
    
}



    