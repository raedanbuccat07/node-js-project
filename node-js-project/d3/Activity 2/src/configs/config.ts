import dotenv from 'dotenv';

dotenv.config();

const DEFAULTS = {
    HOST_NAME: process.env.HOST_NAME || '127.0.0.1',
    PORT: Number(process.env.PORT) || 3000
};

export default {
    DEFAULTS
};