import express from 'express';
import helmet from 'helmet';
import config from './configs/config';
import router from './routes/index.routes';

//server setup
const app = express();
app.use(helmet());

app.get('/', (req, res, next) => {
    res.send("hello world");
    next();
})

app.use(express.json());
app.use('/api', router);

app.listen(config.DEFAULTS.PORT, config.DEFAULTS.HOST_NAME, () => {
    console.log(`Server running at http://${config.DEFAULTS.HOST_NAME}:${config.DEFAULTS.PORT}`)
});