import { Request, Response, NextFunction } from "express";

export class UserController {
    static add = async (req: Request, res: Response, next: NextFunction) => {
        //Add logic
        return res.status(201).json({ msg: "successfully added!", user: req.body});
    }
}