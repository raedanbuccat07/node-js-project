import { Router } from 'express';

const router = Router();

router.get('/',
    (req, res, next) => {
        if (req.headers.authorization === 'secretp@ssw0rd') {
            next();
        } else {
            res.status(401).send('Not permitted.');
        }
    },
    (req, res, next) => {
        if (req.headers.access === 'profile') {
            next();
        } else {
            res.status(401).send('Not permitted.');
        }
    },
    (req, res) => {
    res.send("Welcome!");
})


export default router;