import { Router } from 'express';

const router = Router();

const users = [
    {
        id: 1,
        name: 'John',
        age: 20
    },
    {
        id: 2,
        name: 'John',
        age: 20
    },
    {
        id: 3,
        name: 'John',
        age: 20
    },
    {
        id: 4,
        name: 'John',
        age: 20
    },
]

router.get('/', (req, res) => {
    res.send('Hello World!');

});

router.get('/', (req, res) => {
    res.json(users);
})

router.get('/:userId', (req, res) => {
    //console.log(req.params.userId);
    const user = users.find(user => user.id === Number(req.params.userId))
    //if (!user) res.status(404).send();
    if (!user) res.status(404).json({ msg: 'User not found.' });
    res.json(user);
})


router.post('/', (req, res) => {
    //Add logic
    res.status(201).json({ msg: "successfully added!", user: req.body});
})

router.patch('/:userId', (req, res) => {
    const user = users.find(user => user.id === Number(req.params.userId))
    if (!user) res.status(404).json({ msg: 'User not found.' });
    //Add logic

    res.status(200).json({ msg: "successfully updated!", user: req.body});
})

router.delete('/:userId', (req, res) => {
    const user = users.find(user => user.id === Number(req.params.userId))
    if (!user) res.status(404).json({ msg: 'User not found.' });
    //Add logic

    res.status(204).send();
})

export default router;