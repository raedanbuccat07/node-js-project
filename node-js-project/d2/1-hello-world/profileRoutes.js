import { Router } from 'express';

const router = Router();

router.get('/profile',
    (req, res, next) => {
        if (req.headers.authorization === 'secretp@ssw0rd') {
            next();
        } else {
            res.status(401).send('Not permitted.');
        }
    },
    (req, res, next) => {
        if (req.headers.access === 'profile') {
            next();
        } else {
            res.status(401).send('Not permitted.');
        }
    },
    (req, res) => {
    res.send("Welcome!");
})


module.exports = router;