import express from 'express';

//Routes
import userRoutes from './userRoutes.js';
import userRoutes from './profileRoutes.js';

//server setup
const HOST_NAME = '127.0.0.1';
const PORT = 3000;
const app = express();

//Allows all resources/origin to access our backend application
app.use(express.json());

//Routes
//app.use(bodyParser.json());
app.use('/api/users', userRoutes)
app.use('/assets', express.static(`dist/assets`))

app.get(/.*fly$/, (req, res) => {
    res.send('Fly High!');
});

app.use((err, req, res, next) => {
    console.log(err);
    res.status(503).send("something broke");
})

app.listen(PORT, HOST_NAME, () => {
    console.log(`Server running at http://${HOST_NAME}:${PORT}`)
});